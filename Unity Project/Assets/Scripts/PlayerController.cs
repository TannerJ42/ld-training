﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{


	// health info
	public int maxHealth = 3;
	public int health;
	public float knockbackForce = 500f;

	// lives
	public int maxLives = 3;
	public int lives;

	// invincibility
	public float invincibleTime = 1.5f;
	float invincibleTimer = 0;
	public float stunOnDamageTime = 0.5f;
	float stunOnDamageTimer = 0;

	// move and jump info
	bool facingRight = false;
	public float maxSpeed = 50f;
	public float jumpStrength = 1200f;
	public Transform groundCheck;
	float xDirection = 0f;
	float groundRadius = 0.2f;
	public LayerMask groundLayer;
	bool grounded = false;

	// shoot things
	EyeController eyeController;
	public float laserRechargeTime = 0.5f;
	float laserTimer;


	Animator anim;

	bool Invincible
	{
		get
		{
			return (invincibleTimer > 0);
		}
	}

	bool Stunned
	{
		get
		{
			return (stunOnDamageTimer > 0);
		}
	}

	// Use this for initialization
	void Start() 
	{
		anim = gameObject.GetComponentInChildren<Animator>();
		eyeController = gameObject.GetComponentInChildren<EyeController>();

		health = maxHealth;

		Flip();
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (laserTimer > 0)
		{
			laserTimer -= Time.deltaTime;
		}
		if (invincibleTimer > 0)
		{
			invincibleTimer -= Time.deltaTime;
		}
		if (stunOnDamageTimer > 0)
		{
			stunOnDamageTimer -= Time.deltaTime;
		}

		if (laserTimer <= 0 &&
			Input.GetButton("Fire1"))
		{
			eyeController.FireLaser();
			laserTimer = laserRechargeTime;
		}
	}

	void FixedUpdate()
	{
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, groundLayer);

		Move();

		if (Input.GetButtonDown("Jump") ||
		    Input.GetKeyDown("w"))
		{
			Jump();
		}

		anim.SetBool("Grounded", grounded);
		anim.SetFloat("YVelocity", Input.GetAxis ("Vertical"));
	}

	void Jump()
	{
		if (grounded && 
		    !Stunned)
			rigidbody2D.AddForce(new Vector2(0, jumpStrength));
	}

	void Move()
	{
		if (Stunned)
			return;

		xDirection = Input.GetAxisRaw("Horizontal");

		anim.SetFloat("Speed", Mathf.Abs(xDirection));

//		// custom physics to handle drag cause derrick hates me
//			if (grounded &&
//				xDirection == 0)
//			{
//				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x * 0.1f, rigidbody2D.velocity.y); // derrick wants player to stop faster
//			}
//
//			if (xDirection < 0 &&
//			    facingRight)
//			{
//			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x * 0.1f, rigidbody2D.velocity.y); // derrick wants player to stop faster
//			}
//			else if (xDirection > 0 &&
//			    !facingRight)
//			{
//			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x * 0.1f, rigidbody2D.velocity.y); // derrick wants player to stop faster
//			}

//	    rigidbody2D.AddForce(new Vector2(xDirection * maxSpeed, 0));

		rigidbody2D.velocity = new Vector2(xDirection * maxSpeed, rigidbody2D.velocity.y);

		if (facingRight && xDirection < 0)
			Flip ();
		else if (!facingRight && xDirection > 0)
			Flip ();
	}

	public void TakeDamage(int damage, Vector2 center)
	{
		if (Invincible)
			return;

		rigidbody2D.velocity = Vector2.zero;
		
		Vector2 direction = (Vector2)transform.position - center;
		direction.Normalize();

		if (direction.y < 0.5f) // we want the player to be knocked upward at least a little.
			direction.y = 1f;
		
		rigidbody2D.AddForce(direction * knockbackForce);

		stunOnDamageTimer = stunOnDamageTime;

		health -= damage;
		invincibleTimer = invincibleTime;
		if (health <= 0)
			Die();
		else
			anim.SetTrigger("TakeDamage");

	}

	void Flip()
	{
		facingRight = !facingRight;

		if (facingRight)
			transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
		else
			transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
	}

	void Die()
	{
		Destroy (gameObject); // for now
	}
}
