﻿using UnityEngine;
using System.Collections;

public class EyeController : MonoBehaviour 
{
	public Transform Pupil;
	public GameObject Laser;
	public float pupilRadius = 1f;

	Vector2 direction;

	// Use this for initialization
	void Start() 
	{
		
	}
	
	// Update is called once per frame
	void Update() 
	{
		Vector2 center = transform.position;
		direction = (Vector3)Camera.main.ScreenToWorldPoint(Input.mousePosition) - (Vector3)transform.position;
		direction.Normalize();

		Vector3 newPosition = center + pupilRadius * direction;
		Pupil.transform.position = new Vector3(newPosition.x, newPosition.y, Pupil.transform.position.z);	
	}

	public void FireLaser()
	{
		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
		Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		
		LaserController laserInstance = ((GameObject)Instantiate(Laser, this.transform.position, rotation)).GetComponent<LaserController>();
		laserInstance.direction = direction;
	}
}
