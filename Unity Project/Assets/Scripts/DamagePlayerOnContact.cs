﻿using UnityEngine;
using System.Collections;

public class DamagePlayerOnContact : MonoBehaviour 
{
	public int damage = 1;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerStay2D(Collider2D c)
	{
		if (c.tag == "Player")
		{
			PlayerController p = c.gameObject.GetComponent<PlayerController>();
			p.TakeDamage(damage, this.transform.position);
		}
	}
}
