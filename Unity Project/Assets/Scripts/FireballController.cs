﻿using UnityEngine;
using System.Collections;

public class FireballController : MonoBehaviour 
{
	public int damage = 1;
	public float speed = 1f;
	public Vector2 direction = new Vector2(1, 0);
	
	// Use this for initialization
	void Start() 
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, 0.02f); // hard code z so it isn't overwritten when instantiating.

		Debug.Log (transform.rotation.z * Mathf.Deg2Rad);

		float radians = transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
		direction = new Vector2 ((float)Mathf.Cos (radians), (float)Mathf.Sin (radians));
	}
	
	// Update is called once per frame
	void Update() 
	{
		transform.position = transform.position + (Vector3)direction * speed * Time.deltaTime;
	}
	
	void FixedUpdate()
	{
		
	}
	
	void OnTriggerEnter2D(Collider2D c)
	{
		if (c == null)
			return;

		Debug.Log ("In OnTriggerEnter2D with tag " + c.tag);
		
		if (c.tag == "Player")
		{
			Destroy (gameObject);

			PlayerController p = c.gameObject.GetComponent<PlayerController>();

			p.TakeDamage(damage, this.transform.position);

			Destroy (this.gameObject);
		}
		else if (c.tag == "Environment")
		{
			Destroy (this.gameObject);
		}
	}
}
