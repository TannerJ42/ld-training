﻿using UnityEngine;
using System.Collections;

public class FlamethrowerController : MonoBehaviour 
{
	public GameObject flame;

	public float timeBetweenFlames = 3f;
	public float flameDuration = 1f;

	float flameTimer;

	bool flameActive
	{
		get
		{
			return flame.activeInHierarchy;
		}
	}

	// Use this for initialization
	void Start() 
	{
		flame.SetActive(false);
		flameTimer = timeBetweenFlames;
	}
	
	// Update is called once per frame
	void Update() 
	{
		if (flameTimer > 0)
		{
			flameTimer -= Time.deltaTime;
		}

		if (flameTimer <= 0)
		{
			if (flameActive)
			{
				flame.SetActive(false);
				flameTimer = timeBetweenFlames;
			}
			else
			{
				flame.SetActive(true);
				flameTimer = flameDuration;
			}
		}
	}
}
