﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour 
{
	public GameObject[] hpBlocks;
	//public GameObject[] lifeBlocks;

	PlayerController hero;

	//int currentLives;
	int currentHealth;

	// Use this for initialization
	void Start () 
	{
		hero = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (currentHealth != hero.health)
		{
			SetHealth();
		}
	}

	void SetHealth()
	{
		currentHealth = hero.health;

		for (int i = hpBlocks.Length -1; i >= 0; i--)
		{
			if (currentHealth > i)
				hpBlocks[i].SetActive(true);
			else
				hpBlocks[i].SetActive(false);
		}
	}
}
