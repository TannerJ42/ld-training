﻿using UnityEngine;
using System.Collections;

public class DragonHeadController : MonoBehaviour 
{
	public GameObject fireballPrefab;
	public GameObject fireballLaunchPoint;
	public float[] fireballTimes = new float[]{1.0f};

	int fireballTimerIndex = 0;
	float fireballTimer;

	// Use this for initialization
	void Start() 
	{
	    if (fireballTimes.Length == 0) 
		{
			Debug.Log("Set some timers!");
			gameObject.SetActive(false);
			return;
		}

		fireballTimer = fireballTimes[0];
	}
	
	// Update is called once per frame
	void Update() 
	{
		fireballTimer -= Time.deltaTime;

		if (fireballTimer <= 0)
			ShootFireball();
	}

	void ShootFireball()
	{
		Instantiate(fireballPrefab, fireballLaunchPoint.transform.position, this.transform.rotation);
		++fireballTimerIndex; 
		fireballTimerIndex %= fireballTimes.Length;
		fireballTimer = fireballTimes [fireballTimerIndex];
	}
}
