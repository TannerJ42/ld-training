﻿using UnityEngine;
using System.Collections;

public class LaserController : MonoBehaviour 
{
	public float speed = 2f;
	public Vector2 direction = new Vector2(1, 0);

	// Use this for initialization
	void Start() 
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, 0.02f); // hard code z so it isn't overwritten when instantiating.
	}
	
	// Update is called once per frame
	void Update() 
	{
		transform.position = transform.position + (Vector3)direction * speed * Time.deltaTime;
	}

	void FixedUpdate()
	{

	}

	void OnTriggerEnter2D(Collider2D c)
	{
		if (c == null)
			return;

		Debug.Log ("In OnTriggerEnter2D with tag " + c.tag);

		if (c.tag == "Environment")
			Destroy (gameObject);
	}
}
